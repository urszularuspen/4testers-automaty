def get_player_description(data):
    source = data.get("source", "Steam")
    print(f'The player {data["nick"]} is of type {data["type"]} and has {data["exp_points"]} EXP')
    print(f'Additional data {source}')


def check_cat_age(cat_age):
    if cat_age > 1 and (cat_age - 1) % 3 == 0:
        return True
    else:
        return False


def check_dog_age(dog_age):
    if dog_age > 1 and (dog_age - 1) % 2 == 0:
        return True
    else:
        return False


def check_if_vaccination_is_needed(pet_kind, pet_age):
    if pet_age > 0 and pet_age <= 1:
        return True
    elif pet_kind == "kot":
        return check_cat_age(pet_age)
    elif pet_kind == "pies":
        return check_dog_age(pet_age)
    else:
        raise Exception("An error occurred")


if __name__ == '__main__':
    player1_details = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }
    get_player_description(player1_details)

    print(check_if_vaccination_is_needed("kot", 1))
    print(check_if_vaccination_is_needed("kot", 2))
    print(check_if_vaccination_is_needed("kot", 3))
    print(check_if_vaccination_is_needed("pies", 1))
    print(check_if_vaccination_is_needed("pies", 2))
    print(check_if_vaccination_is_needed("pies", 3))
    print(check_if_vaccination_is_needed("chomik", 3))
