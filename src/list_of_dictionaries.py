animal = {
    "kind": "dog",
    "age": 2,
    "male": True
}

animal2 = {
    "kind": "cat",
    "age": 5,
    "male": False
}

animal_kinds = ["dog", "cat", "fish"]

animals = [
    {
        "kind": "dog",
        "age": 2,
        "male": True
    },
    {
        "kind": "dog",
        "age": 2,
        "male": True
    },
    {
        "kind": "fish",
        "age": 1,
        "male": True
    }
]

print(len(animals))
print(animals[0])
print(animals[-1]["kind"])

for element in animals:
    print(element["kind"])

last_animal = animals[-1]
last_animal_age = last_animal["age"]
print(f"The age od the last animal is equal to {last_animal_age}")

animals.append(
    {
        "kind": "zebra",
        "age": 3,
        "male": False
    }
)

print(animals)
