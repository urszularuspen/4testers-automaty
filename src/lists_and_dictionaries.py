# lists
shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'washing liquid']
print(shopping_list[0])
print(shopping_list[1])
print(shopping_list[-3])
shopping_list.append('lemons')
print(shopping_list)
print(shopping_list[-1])
shopping_list.i

number_of_shopping_list_items = len(shopping_list)
print(number_of_shopping_list_items)

first_three_shopping_list_items = shopping_list[0:3]
print(first_three_shopping_list_items)

# pop - usuwa ostatni element pop(-2)
# insert
# index

# dictionaries - maja klucze i wartosci
animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 7,
    "male": True
}

dog_age = animal["age"]
print("Dog age:", dog_age)
dog_name = animal["name"]
print("Dog name:", dog_name)

animal["age"] = 10
print(animal)

animal["owner"] = "Staszek"
print(animal)