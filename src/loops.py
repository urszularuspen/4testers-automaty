import random


def print_ten_numbers():
    for i in range(10):
        print(i)


def print_even_numbers_in_range():
    for i in range(0, 21, 2):
        print(i)


def print_numbers_from_one_to_thirty_divisible_by_seven_in_range():
    for i in range(1, 31):
        if i % 7 == 0:
            print(i)


def print_random_numbers(n):
    for i in range(n):
        print(random.randint(100, 999))


def generate_list_of_ten_random_numbers_from_1000_to_5000():
    numbers = []
    for i in range(10):
        numbers.append(random.randint(1000, 5000))
        return numbers


def print_each_number_in_the_list_squared(input_list):
    for number in input_list:
        print(number ** 2)


def convert_celsius_to_fahrenheit(temperature_in_celsius):
    return temp_in_celsius * 9 / 5 + 32
def convert_temperature_in_celsius_to_fahrenheit(celsius_list):
    fahrenheit_list = []
    for temperature in celsius_list:
        temperature_in_fahrenheit = convert_celsius_to_fahrenheit(temperature)
        fahrenheit_list.append(temperature_in_fahrenheit)
        return fahrenheit_list




if __name__ == '__main__':
    print_ten_numbers()
    print_even_numbers_in_range()
    print_numbers_from_one_to_thirty_divisible_by_seven_in_range()
    print_random_numbers(4)
    print(generate_list_of_ten_random_numbers_from_1000_to_5000())
    numbers = [1, 5, 6454, 656, 3434]
    print_each_number_in_the_list_squared(numbers)
    my_list = [10.3, 23.2, 15.8]
    print(convert_temperature_in_celsius_to_fahrenheit(my_list))
