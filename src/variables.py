friends_name = "Michał"
friends_age = 41
friends_pet_number = 0
has_driving_license = True
friendship_years = 36.5

print("Name:", friends_name, sep="+")
print("Age:", friends_age)
print("Has driving licence:", has_driving_license, sep="\t\t")
print("Pet amount:", friends_pet_number, sep="\n")
print("Friendship years:", friendship_years)

print("__________________________________________")
print("Name:", friends_name, "Age", friends_age, "Pet number", friends_pet_number, sep="\n")

print(
    "Name:", friends_name,
    "Age", friends_age,
    "Pet number", friends_pet_number,
    sep="\n"
)

name_surname = "Urszula Maciąg"
email = "uma@gmail.com"
phone_number = "+48000000000"

print("Name:", name_surname, "\nEmail", email)

# konkatenacja
bio = "Name: " + name_surname + "\nEmail: " + email + "\nPhone number: " + phone_number
print("bio:" + bio)

# f string
bio_smarter = f"Name: {name_surname}\nEmail: {email}\nPhone: {phone_number}"
print("bio_smarter:" + bio_smarter)

# doc string
bio_docstring = f"""Name: {name_surname}
Email: {email}
Phone: {phone_number}"""
print("doc_string:" + bio_docstring)

physics_lecture = f"The energy of an object with mass 1g is equal to {0.001*300_000_000**2} ev"
print(physics_lecture)
# ** do kwadratu
physics_lecture_1 = f"The energy of an object with mass 1g is equal to {0.001*3e8}"
print(physics_lecture_1)

physics_lecture_3 = f"The energy of an object with mass 1g is equal to {round(0.001*300_000_000**2)} ev"
print(physics_lecture_3)


