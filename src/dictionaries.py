if __name__ == '__main__':
    my_pet = {
        "name": "Chrupek",
        "kind": "puppy",
        "age": 1,
        "weight": 7,
        "is_male": False,
        "favourite food": ["milk", "cola", "gummies"]
    }

    print(my_pet["weight"])
    my_pet["weight"] = 5
    my_pet["likes_swimming"] = False

    del my_pet["is_male"]
    print(my_pet)

    my_pet["favourite food"].append("choco")
    print(my_pet)


    def get_person_data_dictionary(email, phone, city, street):
        return {
            "contact": {
                "email": email,
                "phone": phone},
            "address": {
                "city": city,
                "street": street}
        }


    print(get_person_data_dictionary("u@gmail.com", 99999, "Warsaw", "dluga"))
