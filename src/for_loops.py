def print_each_student_name_capitalized(list_of_students_first_names):
    for first_name in list_of_students_first_names:
        print(first_name.capitalize())


def print_first_ten_integers_squared():
    # first_ten_integers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # for integer in first_ten_integers:
    for integer in range(1, 11):
        print(integer ** 2)


def print_integers_squared(list_of_integers):
    for integer in list_of_integers:
        print(integer ** 3)


if __name__ == '__main__':
    list_of_students = ["kate", "marek", "tosia", "Nicki", "stef"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_integers_squared()
    list_of_random_numbers = [1, 3]
    print_integers_squared(list_of_random_numbers)

# for i in range(10)
# blok wykonany 10 razy

# for name in names:
#
