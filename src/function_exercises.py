def print_a_car_brand_name():
    print("Honda")


def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)


def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2


def calculate_area_of_a_triangle(bottom, height):
    return 0.5 * bottom * height


def upper_word(word):
    return word.upper()


def add_two_numbers(a, b):
    return a & b


def calculate_square_of_number(number):
    return number ** 2


def calculate_cuboid_volume(a, b, c):
    return a * b * c


def convert_celsius_to_fahrenheit(temp_in_celcius):
    return temp_in_celcius * 9 / 5 + 32


def print_hello_sentence(name, city):
    print(f"Witaj {name.upper()}! Miło Cię widzieć w namszym mieście: {city.upper()}!")


def generate_email_address_in_4testers_pl_domain(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


def create_email_for_user_in_domain(prefix, domain="w@pl"):
    return f"{prefix}@{domain}"


def generate_user_contact_data_dictionary(email, phone_number, country_code="+48", country="Poland"):
    return {
        "email": email,
        "phone_number": f"{country_code}{phone_number}",
        "country": country
    }


def calculate_average_grade(grade_list):
    return sum(grade_list) / len(grade_list)


if __name__ == '__main__':
    print_a_car_brand_name()
    print_given_number_multiplied_by_3(10)
    area_of_a_circle_with_a_radius_10 = calculate_area_of_a_circle(10)
    print(area_of_a_circle_with_a_radius_10)
    area_of_a_little_triangle = calculate_area_of_a_triangle(10, 5)
    print(area_of_a_little_triangle)
    # Checking result of a print function
    # result_of_a_print_function = print_a_car_brand_name()
    # print(result_of_a_print_function)
    big_dog = upper_word("dog")
    print(big_dog)
    print(upper_word("5"))
    print(add_two_numbers(3, 5))
    print(add_two_numbers(3, True))
    print(calculate_square_of_number(16.1))
    print(calculate_cuboid_volume(3, 5, 7))
    print(convert_celsius_to_fahrenheit(20))
    print_hello_sentence("ula", "Kraków")
    grades = [2, 3, 4, 5, 6]
    print(calculate_average_grade(grades))
    print(create_email_for_user_in_domain("ula", "g.pl"))
    print(create_email_for_user_in_domain("ula", ))

    print("pies", "kot", "zaba", sep=":")
    print(generate_user_contact_data_dictionary("ula@g.pl", phone_number="999999", country="+42"))
